defmodule Aoc do
  defmodule Season2016 do
    defmodule Day1 do
      def results(part, instructions) do
        origin = %{ north: 0, east: 0, direction: :north }
        steps = steps(instructions)
        destination = steps
          |> Enum.reduce( origin, fn(step, position) -> position |> rotate(step.direction) |> walk(step.length)  end )

        case part do
          :part_1 -> distance(destination, origin)
          :part_2 -> distance(first_cross_path(origin, steps), origin)
        end
      end

      def steps(instructions) do
        String.split(instructions, ",")
          |> Enum.map(&step/1)
          |> Enum.reject( fn(step) -> step == :no_instruction end )
      end

      def step(instruction) do
        case Regex.scan(~r/\s*([R|L])(\d+)/, instruction) do
          [[_, d, l]] -> %{ direction: d, length: String.to_integer(l) }
          _ -> :no_instruction
        end
      end

      def turn(direction, "R") do
        case direction do
          :north -> :east
          :east -> :south
          :south -> :west
          :west -> :north
        end
      end

      def turn(direction, "L") do
        case direction do
          :north -> :west
          :east -> :north
          :south -> :east
          :west -> :south
        end
      end

      def rotate(position, direction) do
        %{ position | direction: turn(position.direction, direction) }
      end

      def walk(position, steps_count) do
        delta = case position.direction do
          :north -> %{ change: :north, orientation: 1 }
          :east -> %{ change: :east, orientation: 1 }
          :south -> %{ change: :north, orientation: -1 }
          :west -> %{ change: :east, orientation: -1 }
        end
        %{ position | delta.change => position[delta.change] + steps_count * delta.orientation }
      end

      def distance(destination, origin) do
        abs(destination.north - origin.north) + abs(destination.east - origin.east)
      end

      def baby_order(step) do
        [%{ rotate: step.direction } | Enum.map(1..step.length, fn(_) -> %{ walk: 1 } end) ]
      end

      def first_cross_path(origin, steps) do
        translator = fn(order, walked) ->
          case order do
            %{ rotate: direction } ->
              { :cont, walked ++ [rotate(List.last(walked), direction)] }
            %{ walk: length } ->
              destination = walk(List.last(walked), length)
              case Enum.find(walked, fn(position) -> destination.north == position.north and destination.east == position.east end ) do
                nil -> { :cont,  walked ++ [destination]}
                _ -> { :halt,  [destination]}
              end
          end
        end
        Enum.reduce_while(Enum.flat_map(steps, &baby_order/1), [origin], translator) |> List.last
      end
    end
  end
end
