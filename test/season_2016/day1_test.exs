defmodule Aoc.Season2016.Day1Test do
  use ExUnit.Case

  test "without input the part_1 shows no steps" do
    assert Aoc.Season2016.Day1.results(:part_1, "")== 0
  end

  test "with a single step the distance is the step count" do
    assert Aoc.Season2016.Day1.results(:part_1, "L1")== 1
    assert Aoc.Season2016.Day1.results(:part_1, "R1")== 1
    assert Aoc.Season2016.Day1.results(:part_1, "R3")== 3
    assert Aoc.Season2016.Day1.results(:part_1, "L723")== 723
  end

  test "with a multi step the distance is the sum of the steps" do
    assert Aoc.Season2016.Day1.results(:part_1, "L1, R1")== 2
    assert Aoc.Season2016.Day1.results(:part_1, "R1, R3")== 4
    assert Aoc.Season2016.Day1.results(:part_1, "R2, R2, R2")== 2
  end

  test "with cross the same path without movement" do
    assert Aoc.Season2016.Day1.results(:part_2, "") == 0
  end

  test "with cross the same path reaching the origin" do
    assert Aoc.Season2016.Day1.results(:part_2, "R2, R2, R2, R2") == 0
  end

  test "with instructions that cross the already transited path" do
    assert Aoc.Season2016.Day1.results(:part_2, "R8, R4, R4, R8") == 4
  end
end
